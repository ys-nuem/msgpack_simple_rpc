﻿#include "stdafx.h"
#include <rpc.hpp>
using namespace std;


int main()
{
    try {
        // サーバとの接続を確立する
        auto client = rpc::make_client("127.0.0.1", 31400);

        // コマンドの登録
        client.set_callback(
            /*command=*/"Hello",
            /*callback=*/[](string const&) {
                cout << "received `Hello`." << endl;
                this_thread::sleep_for(chrono::seconds(3));
                cout << "exit application." << endl;
                exit(0);
            }
        );

        // サービスの開始
        client.run();
    }
    catch (rpc::error_code const& err)
    {
        cerr << "An exception was thrown." << endl;
        cerr << err.message() << endl;
        terminate();
    }
    catch (exception const& err)
    {
        cerr << "An exception was thrown." << endl;
        cerr << err.what() << endl;
        terminate();
    }
    catch (...)
    {
        cerr << "An unknown exception was thrown." << endl;
        terminate();
    }

    return 0;
}