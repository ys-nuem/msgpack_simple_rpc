#pragma once

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <chrono>
#include <functional>

#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4267)
#include <boost/asio.hpp>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable:4127)
#pragma warning(disable:4267)
#pragma warning(disable:4512)
#include <msgpack.hpp>
#pragma warning(pop)