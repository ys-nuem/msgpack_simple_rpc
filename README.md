Yet another RPC using Messagepack
=================================

[MessagePack](http://msgpack.org/) によるRPCの実装例

# Requirements

* Microsoft(R) Visual Studio 2013
* Boost.Asio
* Messagepack for C/C++ ver 1.1.0

# Usage

## Server side

```cpp
#include <string>
#include <vector>
#include <rpc.hpp>

int add(rpc::object const& args) {
    // コールバック関数ごとに引数を解析する
    int a1, a2;
    rpc::unpack_args(args, a1, a2); // may throws rpc::error_code.

    return a1 + a2;
}

int main()
{
    // Listener port
    static const unsigned short LISTEN_PORT = 31400;
    
    // サーバのインスタンスを生成する
    rpc::server server = rpc::make_server(LISTEN_PORT);

    // コールバック関数を登録する
    server.response("add") = add;
    server.response("answer") = []{ return 42; };
    
    server.run();
    return 0;
}
```

## Client side

```cpp
#include <iostream>
#include <string>
#include <rpc.hpp>

int main()
{
    // サーバ情報
    static const std::string ADDRESS = "127.0.0.1";
    static const unsigned short PORT = 31400;
    
    // クライアントのインスタンス生成
    rpc::client client = rpc::make_client(ADDRESS, PORT);
    
    // RPC経由で関数の結果を受け取る
    rpc::object response = client.request("add", 1, 2); // 結果はrpc::objectで返る
                                                        // サーバ側でコマンド実行時に例外が送出された場合は
                                                        // その例外を送出する
    int add = response.as<int>();                       // クライアント側で指定した型に変換

    int answer = client.request("answer").as<int>();    // コマンドの引数は省略可能
    
    // 非同期実行の例
    std::future<int> ans2 = std::async([&]{ return client.request("answer").as<int>(); });
    // ... do_something();
    std::cout << "anser = " << ans2.get() << std::endl;
}
```

## License

Copyright (c) 2015 Yusuke Sasaki.

This software is released under the Apache License Version 2.0, see [LICENSE](LICENSE).
