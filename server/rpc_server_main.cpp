﻿#include "stdafx.h"
#include <rpc.hpp>
using namespace std;


int main()
{
    static const unsigned short PORT = 31400;
    static const std::size_t NUM_CLIENTS = 3;
    try {
        auto io = std::make_shared<rpc::io_service>();
        auto server = rpc::make_server(std::move(io), PORT, NUM_CLIENTS);
        // or simply:
        // auto server = rpc::make_server(PORT, NUM_CLIENTS);

        for (size_t i = 0; i < server.num_clients(); ++i)
        {
            // send message to client.
            server.send_message(i, "Hello");
        }
    }
    catch (rpc::error_code const& err)
    {
        cerr << "An exception was thrown." << endl;
        cerr << err.message() << endl;
        terminate();
    }
    catch (exception const& err)
    {
        cerr << "An exception was thrown." << endl;
        cerr << err.what() << endl;
        terminate();
    }
    catch (...)
    {
        cerr << "An unknown exception was thrown." << endl;
        terminate();
    }

    return 0;
}