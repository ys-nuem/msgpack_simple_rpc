﻿#pragma once

#include <iostream>
#include <functional>
#include <memory>
#include <vector>
#include <map>
#include <boost/asio.hpp>

namespace rpc {

namespace asio = boost::asio;
namespace ip = asio::ip;
using boost::asio::io_service;
using boost::asio::ip::tcp;
using acceptor = boost::asio::ip::tcp::acceptor;
using boost::system::error_code;


class server 
{
  public:
    server(std::shared_ptr<io_service> io, std::vector<tcp::socket> && sockets)
        : sockets_(std::move(sockets))
        , io_(std::move(io))
    {
    }

    server(server && other)
        : sockets_(std::move(other.sockets_))
        , io_(std::move(other.io_))
    {
    }

    server& operator=(server && other)
    {
        sockets_.clear();
        std::swap(sockets_, other.sockets_);
        std::swap(io_, other.io_);
        return *this;
    }

    std::size_t num_clients() const { return sockets_.size(); }

    void send_message(std::size_t i, std::string const& message)
    {
        asio::write(sockets_[i], asio::buffer(message));
    }

  private:
    server(server const&) = delete;
    server& operator=(server const&) = delete;

  private:
    std::vector<tcp::socket>    sockets_;
    std::shared_ptr<io_service> io_; 
};


server make_server(std::shared_ptr<io_service> io, unsigned short port,
                   std::size_t num_connections = 3)
{
    acceptor acc(*io, tcp::endpoint(tcp::v4(), port));

    std::vector<tcp::socket> clients;
    for (size_t i = 0; i < num_connections; ++i)
    {
        tcp::socket socket(*io);
        acc.accept(socket);
        clients.emplace_back(std::move(socket));
    }
    return server(std::move(io), std::move(clients));
}


server make_server(unsigned short port, std::size_t num_connections = 3)
{
    return make_server(std::make_shared<io_service>(), port, num_connections);
}


class client
{
  public:
    client(std::shared_ptr<io_service> io, tcp::socket && socket)
        : io_(std::move(io))
        , socket_(std::move(socket))
    {
    }

    client(client && other)
        : io_(std::move(other.io_))
        , socket_(std::move(other.socket_))
        , callback_(std::move(other.callback_))
    {
    }

    client& operator=(client && other)
    {
        io_.reset();
        std::swap(io_, other.io_);
        std::swap(socket_, other.socket_);
        std::swap(callback_, other.callback_);
        return *this;
    }
    
    void run()
    {
        for (;;) {
            std::string cmd = receive_message();
            if (callback_.count(cmd) > 0) {
                callback_.at(cmd)("empty");
            }
        }
    }

    template <class Callback>
    void set_callback(std::string const& cmd, Callback callback)
    {
        callback_[cmd] = std::move(callback);
    }

    std::string receive_message()
    {
        asio::streambuf rec_buf;
        error_code err;
        asio::read(socket_, rec_buf, asio::transfer_all(), err);
        if (err && err != asio::error::eof) { 
            std::cerr << "failed to receive" << std::endl;
            std::cerr << err.message() << std::endl;
            throw err;
        }
        return asio::buffer_cast<char const*>(rec_buf.data());
    }

  private:
    client(client const&) = delete;
    client& operator=(client const&) = delete;

  private:
    std::shared_ptr<io_service> io_;
    tcp::socket                 socket_;
    std::map<std::string, std::function<void(std::string const&)>> callback_;
};


client make_client(std::string const& address, unsigned short port)
{
    auto io = std::make_shared<io_service>();

    tcp::socket socket(*io);
    auto addr = tcp::endpoint(ip::address::from_string(address), port);
    error_code err;  
    socket.connect(addr, err);
    if (err) {
        std::cerr << "failed to connect to server." << std::endl;
        std::cerr << err.message() << std::endl;
        throw err;
    }
    return client(std::move(io), std::move(socket));
}


} // namespace rpc;
